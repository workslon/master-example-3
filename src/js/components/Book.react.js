var React = require('react');
var Link = require('react-router').Link;
var AppActions = require('../actions/AppActions');
var BookModel = require('../models/Book');

module.exports = React.createClass({
  delete: function () {
    AppActions.deleteBook(BookModel, this.props.book);
  },

  render: function () {
    var book = this.props.book;
    var updatePath = '/update/' + book.objectId;
    var genre = book.genre ? BookModel.properties.genre.range.labels[book.genre - 1] : '';

    return (
      <tr>
        <td scope="row">{this.props.nr}</td>
        <td>{book.isbn}</td>
        <td>{book.title}</td>
        <td>{book.year}</td>
        <td>{book.edition ? book.edition : ''}</td>
        <td>{genre}</td>
        <td className="action-links">
          <Link className="btn btn-primary btn-xs" to={updatePath}>Update</Link>
          <a className="btn btn-danger btn-xs" onClick={this.delete}>Delete</a>
        </td>
      </tr>
    );
  }
});

