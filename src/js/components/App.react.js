var React = require('react');
var IndexLink = require('react-router').IndexLink;
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');
var StatusConstants = require('../constants/StatusConstants');
var ReactRouter = require('react-router');
var BookModel = require('../models/Book');

module.exports = React.createClass({
  getInitialState: function () {
    return {
      books: AppStore.getAllBooks(),
      notifications: AppStore.getNotifications()
    }
  },

  childContextTypes: {
    router: React.PropTypes.object.isRequired
  },

  contextTypes: {
    router: React.PropTypes.object.isRequired
  },

  componentDidMount: function () {
    AppActions.getBooks(BookModel);
    this.context.router.listenBefore(this._clearNotifications);
    AppStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function () {
    AppStore.removeChangeListener(this._onChange);
  },

  _clearNotifications: function () {
    var notifications = this.state.notifications;
    var isStatus = notifications.status !== StatusConstants.IDLE;
    var isErrors = Object.keys(notifications.errors || {}).length;

    if (isStatus || isErrors) {
      AppActions.clearNotifications();
    }
  },

  _onChange: function () {
    this.setState({
      books: AppStore.getAllBooks(),
      notifications: AppStore.getNotifications()
    });
  },

  _renderChildren: function () {
    return React.Children.map(this.props.children, (function (child) {
      return React.cloneElement(child, {
        books: this.state.books,
        notifications: this.state.notifications
      });
    }).bind(this));
  },

  render: function () {
    return (
      <div>
        <header className="well container">
          <h4>Example 3 - Enumerations in User Interfaces</h4>
          <a target="_block" href="https://bitbucket.org/workslon/master-thesis/wiki/chapter5.pdf">Related Thesis Chapter</a>
          &nbsp;/&nbsp;
          <a target="_block" href="https://bitbucket.org/workslon/master-example-3/src">Source Code</a>
        </header>

        <div className="page-header">
          <h1><IndexLink to="/">Public Library</IndexLink></h1>
        </div>
        { this._renderChildren() }
      </div>
    );
  }
});

