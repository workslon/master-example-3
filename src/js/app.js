var React = require('react');
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');

var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var IndexRoute = ReactRouter.IndexRoute;
var hashHistory = ReactRouter.hashHistory;

var App = require('./components/App.react');
var BookList = require('./components/BookList.react');
var CreateBook = require('./components/CreateBook.react');
var UpdateBook = require('./components/UpdateBook.react');

ReactDOM.render((
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={BookList} />
      <Route path="/create" component={CreateBook} />
      <Route path="/update/:id" component={UpdateBook} />
    </Route>
  </Router>
), document.getElementById('app'));