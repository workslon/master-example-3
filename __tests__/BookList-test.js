jest.dontMock('../src/js/components/BookList.react');
jest.dontMock('../src/js/components/Book.react');
jest.dontMock('../src/js/constants/StatusConstants');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');
var StatusConstants = require('../src/js/constants/StatusConstants');

describe('BookList.react', function () {
  var BookList = require('../src/js/components/BookList.react');
  var Link = require('react-router').Link;
  var Book = require('../src/js/components/Book.react');
  var renderer = TestUtils.createRenderer();

  var books = [
    {objectId: 'XXX', isbn: '123456789X', title: 'Book', year: 2000},
    {objectId: 'YYY', isbn: '123456786X', title: 'Book 2', year: 2007}
  ];

  var result;
  var children;

  beforeEach(function () {
    renderer.render(<BookList books={books} />);
    result = renderer.getRenderOutput();
    children = result.props.children;
  });

  it('renders "Add Book" link into book list correctly', function () {
    renderer.render(<BookList books={books} />);
    result = renderer.getRenderOutput();
    children = result.props.children;

    expect(children[0]).toEqual(
      <Link className="create-book btn btn-success bt-sm" to="/create">+ Add Book</Link>
    );
  });

  it('renders book list table correctly', function () {
    renderer.render(<BookList books={books} />);
    result = renderer.getRenderOutput();
    children = result.props.children;

    expect(children[2].type).toEqual('table');
  });

  it('renders books correctly', function () {
    renderer.render(<BookList books={books} />);
    result = renderer.getRenderOutput();
    children = result.props.children;

    expect(children[2].props.children[1].props.children[0].props.book).toEqual(books[0]);
    expect(children[2].props.children[1].props.children[1].props.book).toEqual(books[1]);
  });

  it('shows the pending notification while deleting a book', function() {
    var notifications = {
      status: StatusConstants.PENDING
    };

    renderer.render(<BookList notifications={notifications} books={books} />);
    result = renderer.getRenderOutput();
    children = result.props.children;

    expect(children[1].type).toEqual('p');
    expect(children[1].props.children).toEqual('Deleting...');
  });
});